# README for ClaclaProject

Clacla 

## First thing: 
1. create a directory public/
2. create an index.html in public directory :

```
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Clacla</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="css/styles.css?v=1.0">

  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>

<body>
  Hello Clacla
</body>
</html>
```

3. Add the index.html to Gitlab repository:

```
git add public/index.html
git commit -m "Add index file"
git push
```

Note: 
* Make a git pull if git push is rejected 
* first push command might be 'git push -u origin master'

## Second: set up the project to be a pages project

1. go to Project > Gitlab CI